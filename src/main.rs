use std::error::Error;
use std::path::PathBuf;

use clap::Parser;
use gitlab::api::projects::packages::generic::{GetPackageFileBuilder, UploadPackageFileBuilder};
use gitlab::api::Query;
use gitlab::Gitlab;
use serde_json::Value;

#[derive(Parser)]
struct Command {
    #[arg()]
    file_path: PathBuf,

    #[arg()]
    project_id: String,

    #[arg()]
    token: String,
}

fn main() -> Result<(), Box<dyn Error>> {
    let args = Command::parse();

    let file_name: &str = args.file_path.file_name().unwrap().to_str().unwrap();

    let client: Gitlab = gitlab::GitlabBuilder::new("gitlab.kitware.com", &args.token).build()?;
    let upload_endpoint = UploadPackageFileBuilder::default()
        .project(args.project_id.as_str())
        .package_name("test")
        .package_version("1.0.0")
        .file_name(file_name)
        .select(gitlab::api::projects::packages::generic::UploadPackageSelect::PackageFile)
        .file_path(&args.file_path)
        .build()?;

    let response: Value = upload_endpoint.query(&client)?;

    dbg!(response);

    let get_endpoint = GetPackageFileBuilder::default()
        .project(args.project_id.as_str())
        .package_name("test")
        .package_version("1.0.0")
        .file_name(file_name)
        .build()?;

    let response: Vec<u8> = gitlab::api::raw(get_endpoint).query(&client)?;

    if let Ok(file_contents) = String::from_utf8(response) {
        println!("{file_contents}");
    } else {
        println!("Downloaded binary file");
    }

    Ok(())
}
